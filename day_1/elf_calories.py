import argparse


def elves_get_calories(filename: str = "input.txt") -> dict[int, int]:
    with open(filename) as input_file:
        total_calories = {}
        elf = 1
        elf_calories = 0

        for line in input_file:
            if line.strip():
                elf_calories += int(line)
                total_calories.update({elf: elf_calories})
            else:
                elf += 1
                elf_calories = 0

    return total_calories


def sorted_totals(totals: dict[int, int]) -> list[int]:
    """Return a list of the elves by highest calories."""
    return sorted(totals, key=totals.get, reverse=True)


def top_calories(totals: dict[int, int], top: int = 3) -> int:
    """Return total calories for the ``top`` number of elves."""
    total = 0

    for i in range(top):
        elf_id = sorted_totals(totals)[i]
        total += totals[elf_id]

    return total


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--filename", default="input.txt")
    args = parser.parse_args()

    calories = elves_get_calories(args.filename)

    print(f"elf with highest calories: {top_calories(calories, 1)}")
    print(f"top three elves total: {top_calories(calories)}")
