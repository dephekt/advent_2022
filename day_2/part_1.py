class RockPaperScissors:
    def __init__(self, points: int = None):
        self.points = points

    def __eq__(self, other):
        return True if type(self) == type(other) else False

    def __gt__(self, other):
        if type(self) == Rock:
            return True if type(other) == Scissors else False
        if type(self) == Paper:
            return True if type(other) == Rock else False
        if type(self) == Scissors:
            return True if type(other) == Paper else False


class Rock(RockPaperScissors):
    def __init__(self, points: int = 1):
        super().__init__(points)


class Paper(RockPaperScissors):
    def __init__(self, points: int = 2):
        super().__init__(points)


class Scissors(RockPaperScissors):
    def __init__(self, points: int = 3):
        super().__init__(points)


class Game:
    player_1 = 0
    player_2 = 0

    rounds = 0

    def play_round(self, player_1: RockPaperScissors, player_2: RockPaperScissors):
        self.rounds += 1

        if player_1 == player_2:
            self.player_1 += 3 + player_1.points
            self.player_2 += 3 + player_2.points
        elif player_1 > player_2:
            self.player_1 += 6 + player_1.points
            self.player_2 += player_2.points
        else:
            self.player_1 += player_1.points
            self.player_2 += 6 + player_2.points

        print(f"Round {self.rounds}")
        print(f"Player 1 Score: {self.player_1}")
        print(f"Player 2 Score: {self.player_2}\n")


PLAY_TYPES = {
    "A": Rock(),
    "B": Paper(),
    "C": Scissors(),
    "X": Rock(),
    "Y": Paper(),
    "Z": Scissors(),
}


if __name__ == "__main__":
    game = Game()

    with open("input.txt") as file:
        for line in file:
            plays = line.strip().split(" ")

            game.play_round(
                player_1=PLAY_TYPES[plays[0]],
                player_2=PLAY_TYPES[plays[1]]
            )
