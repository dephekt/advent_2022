from day_2.part_1 import Game, Rock, Paper, Scissors, RockPaperScissors, PLAY_TYPES

STRATEGY_MAPPING = {
    "X": "lose",
    "Y": "draw",
    "Z": "win",
}


def resolve_strategy(us: str, them: RockPaperScissors) -> RockPaperScissors:
    if us == "draw":
        return type(them)()

    if us == "lose":
        if type(them) == Rock:
            return Scissors()
        if type(them) == Paper:
            return Rock()
        if type(them) == Scissors:
            return Paper()

    if us == "win":
        if type(them) == Rock:
            return Paper()
        if type(them) == Paper:
            return Scissors()
        if type(them) == Scissors:
            return Rock()


if __name__ == "__main__":
    game = Game()

    with open("input.txt") as file:
        for line in file:
            strategy = line.strip().split(" ")

            player_1 = PLAY_TYPES[strategy[0]]
            player_2 = resolve_strategy(
                STRATEGY_MAPPING[strategy[1]], player_1
            )

            game.play_round(player_1, player_2)
