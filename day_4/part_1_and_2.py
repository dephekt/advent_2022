#!/usr/bin/env python3
if __name__ == "__main__":
    assignments_complete_intersection = 0
    assignments_any_intersection = 0

    with open("input.txt") as file:
        data = [line.strip() for line in file]

        for pair in data:
            assignment_pairs = pair.split(",")
            pair_1 = assignment_pairs[0]
            pair_2 = assignment_pairs[1]

            assignment_1 = pair_1.split("-")
            assignment_2 = pair_2.split("-")

            assignment_1 = [
                str(i) for i in range(int(assignment_1[0]), int(assignment_1[1]) + 1)
            ]
            assignment_2 = [
                str(i) for i in range(int(assignment_2[0]), int(assignment_2[1]) + 1)
            ]

            intersection = list(set(assignment_1).intersection(assignment_2))
            intersection.sort(key=int)

            if intersection in [assignment_1, assignment_2]:
                assignments_complete_intersection += 1

            if intersection:
                assignments_any_intersection += 1

    print(f"Part 1: {assignments_complete_intersection}")
    print(f"Part 2: {assignments_any_intersection}")
