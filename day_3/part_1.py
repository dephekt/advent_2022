#!/usr/bin/env python3
import string


class Rucksack:
    def __init__(self, sack: str):
        self.contents = sack

    @property
    def first_compartment(self) -> set[str]:
        """Get content of first compartment."""
        return set(self.contents[: len(self.contents) // 2])

    @property
    def second_compartment(self) -> set[str]:
        """Get content of second compartment."""
        return set(self.contents[len(self.contents) // 2 :])

    @property
    def intersection(self) -> set[str]:
        """Get intersection of both compartments."""
        return self.first_compartment.intersection(self.second_compartment)

    @property
    def intersection_priority(self) -> int:
        """Get total priority value of intersection points."""
        total_priority_value = 0

        for point in self.intersection:
            total_priority_value += string.ascii_letters.index(point) + 1

        return total_priority_value


if __name__ == "__main__":
    grand_total_priority = 0

    with open("input.txt") as file:
        for line in file:
            line = line.strip()

            rucksack = Rucksack(line)
            grand_total_priority += rucksack.intersection_priority

    print(grand_total_priority)
