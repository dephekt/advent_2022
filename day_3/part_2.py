#!/usr/bin/env python3
import itertools
import string
import typing


def batched(iterable: typing.Iterable, size: int) -> list[str]:
    it = iter(iterable)
    while batch := list(itertools.islice(it, size)):
        yield batch


if __name__ == "__main__":
    total_priority_value = 0

    with open("input.txt") as file:

        # calculate total priority value of intersection in batches of 3
        rucksacks = [line.strip() for line in file]
        for a, b, c in batched(rucksacks, 3):

            # there can be only one point of intersection between all 3 sets
            intersection = set(a).intersection(b, c).pop()

            # get the ascii letters index of the intersection point
            total_priority_value += string.ascii_letters.index(intersection) + 1

    print(f"total priority value: {total_priority_value}")
